<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package lorenzocarbonell_201711
 */

?>
        </div><!-- .site-content-container -->
    </div><!-- #content -->

    <footer id="colophon" class="site-footer">
        <div class="site-footer-container">
            <div class="site-info">
                <div class="license">
                    <ul>
                        <li><a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><div id="lorenzocarbonell_license"></div></a></li>
                        <li>2018</li>
                        <li><a href="https://gnulinuxvalencia.org/contactar" title="Contactar"><span class="icono email-icon for-colophon"></span>Contactar</a></li>
                    </ul>
                </div>
                <div class="social-links">
                    <ul>
                        <li><a href="https://twitter.com/GnuLinuxVlc" title="Twitter" rollapp-href="https://twitter.com/GnuLinuxVlc"><span class="icono twitter-icon for-colophon"></span>Twitter</a></li>
                        <li><a href="https://www.facebook.com/gnulinuxvalencia" title="Facebook" rollapp-href="https://www.facebook.com/gnulinuxvalencia"><span class="icono facebook-icon for-colophon"></span>Facebook</a></li>
                        <li><a href="https://t.me/gnulinuxvalencia" title="Telegram" rollapp-href="https://t.me/gnulinuxvalencia"><span class="icono telegram-icon for-colophon"></span>Telegram</a></li>
                    </ul>
                </div>
                <div class="contact">
                    <ul>
                        <li><a href="https://floss.social/@gnulinuxvalencia" title="Mastodon" rollapp-href="https://floss.social/@gnulinuxvalencia"><span class="icono mastodon-icon for-colophon"></span>Mastodon</a></li>
                        <li><a href="https://quitter.es/gnulinuxvalencia" title="Quitter" rollapp-href="https://quitter.es/gnulinuxvalencia"><span class="icono gnusocial-icon for-colophon"></span>GNU Social</a></li>
                        <li><a href="https://diasp.org/public/gnulinuxvalencia" title="Diaspora" rollapp-href="https://diasp.org/public/gnulinuxvalencia"><span class="icono diaspora-icon for-colophon"></span>Diáspora</a></li>
                        <!--
                        <li><a href="<?php echo get_site_url();?>/contactar/" title="Contactar" rollapp-href="http://gnulinuxvalencia.org/contactar/"><span class="icono email-icon for-colophon"></span>Contactar</a></li>
                        <li><a href="<?php echo get_site_url();?>/sobre-mi/" title="Quienes somos" rollapp-href="http://gnulinuxvalencia.org/quienes-somos/"><span class="icono info-icon for-colophon"></span>Sobre mi</a></li>
                        <li><a href="<?php echo get_site_url();?>/contactar/" title="Contactar" rollapp-href="http://gnulinuxvalencia.org/concatar/"><span class="icono info-icon for-colophon"></span>Sobre mi</a></li>
                        <li><a href="<?php echo get_site_url();?>/suscribir/" title="Suscribir" rollapp-href="http://gnulinuxvalencia.org/suscribir/"><span class="icono info-icon for-colophon"></span>Sobre mi</a></li>
                        <li><a href="<?php echo get_site_url();?>/donar/" title="Donar" rollapp-href="http://gnulinuxvalencia.org/donar/"><span class="icono info-icon for-colophon"></span>Sobre mi</a></li>
                        -->
                        <!--
                        <li><a href="<?php echo get_site_url();?>/suscribir/" title="Suscribir" rollapp-href="http://lorenzocarbonell.com/suscribir/"><span class="icono pagina-icon for-colophon"></span>Suscribir</a></li>
                        -->
                    </ul>
                </div>
            </div><!-- .site-info-container -->
            <div class="row credits">Un tema para <a href="https://es.wordpress.org/">WordPress</a> de <a rel="author" href="https://lorenzocarbonell.com">Lorenzo Carbonell</a></div>
        </div><!-- .site-info -->
    </footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
