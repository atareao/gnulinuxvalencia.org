<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gnulinuxvalencia
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <?php wp_head(); ?>

    <?php if ( ! is_user_logged_in () ) { ?>
    <!-- Google Analytics On. -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-121550516-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-121550516-1');
        gtag('set', {'user_id': 'USER_ID'}); // Establezca el ID de usuario mediante el user_id con el que haya iniciado sesión.
    </script>
    <?php } else {
    echo '<!-- Google Analytics Off. -->';
    }?>

</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
    <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'gnulinuxvalencia' ); ?></a>

    <header id="masthead" class="site-header">
        <div class="site-header-container">
            <div class="site-branding">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                    <div class="logo"></div>
                    <h1 class="site-title"><?php bloginfo( 'name' ); ?></h1>
                    <?php /*
                    $description = get_bloginfo( 'description', 'display' );
                    if ( $description || is_customize_preview() ) : ?>
                        <p class="site-description"><?php echo $description; // WPCS: xss ok. ?></p>
                    <?php
                    endif; */?>
                </a>
            </div><!-- .site-branding -->

            <nav id="site-navigation" class="main-navigation">
                <ul class="menu-desplegable">
                <li><button class="menu-toggle menu-icon" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'gnulinuxvalencia' ); ?></button></li>
                <li>
                <?php
                    wp_nav_menu( array(
                        'theme_location' => 'menu-1',
                        'menu_id'        => 'primary-menu',
                        'container'      => false,
                    ) );
                ?>
                </li>
                </ul>
            </nav><!-- #site-navigation -->
        </div><!-- .site-header-container -->
    </header><!-- #masthead -->

    <div id="content" class="site-content">
        <div class="site-content-container">
