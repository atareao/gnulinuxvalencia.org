<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package lorenzocarbonell_201711
 */

$postID = get_the_ID();
$season = get_post_meta($postID, 'season', true);
$number = get_post_meta($postID, 'number', true);
$ogg_url = get_post_meta($postID, 'ogg-url', true);
$mp3_url = get_post_meta($postID, 'mp3-url', true);
$nepisodio = 'T'.substr('00'.$season, -2).'E'.substr('00'.$number, -2);
if(isset($ogg_url))
{
    $url = $ogg_url;
}
else if(isset($mp3_url))
{
    $url = $mp3_url;
}
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="entry-header">
        <?php
        if ( is_singular() ) :
            echo '<h1 class="entry-title">'.$nepisodio.' - '.get_the_title().'</h1>';
        else :
            echo '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">'.$nepisodio.' - '.get_the_title().'</a></h2>';
        endif;

        if ( 'post' === get_post_type() ) : ?>
        <div class="entry-meta">
            <?php gnulinuxvalencia_posted_on(); ?>
        </div><!-- .entry-meta -->
        <?php
        endif; ?>
    </header><!-- .entry-header -->

    <div class="entry-content">
        <?php
        if(isset($url)):?>
            <div class="audio-player">
                <audio controls="">
                    <source src="<?php echo $url;?>">
                    Your browser does not support the audio element.
                </audio>
            </div>
        <?php
        endif;
            the_content( sprintf(
                wp_kses(
                    /* translators: %s: Name of current post. Only visible to screen readers */
                    __( 'Continúa leyendo<span class="screen-reader-text"> "%s"</span>', 'gnulinuxvalencia' ),
                    array(
                        'span' => array(
                            'class' => array(),
                        ),
                    )
                ),
                get_the_title()
            ) );

            wp_link_pages( array(
                'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'gnulinuxvalencia' ),
                'after'  => '</div>',
            ) );
        ?>
    </div><!-- .entry-content -->

    <footer class="entry-footer">
        <?php gnulinuxvalencia_entry_footer(); ?>
    </footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
