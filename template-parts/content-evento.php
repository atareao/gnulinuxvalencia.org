<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package lorenzocarbonell_201711
 */
setlocale(LC_ALL,"es_ES");
$postID = get_the_ID();
$number = get_post_meta($postID, 'number', true);
$meeting = get_post_meta($postID, 'meeting', true);
$themes = get_post_meta($postID, 'themes', true);
$where = get_post_meta($postID, 'where', true);
$date = new DateTime(get_post_meta($postID, 'date', true));
$start = get_post_meta($postID, 'start', true);
$end = get_post_meta($postID, 'end', true);

if($where == 2)
{
    $lugar = "Linux Center Valencia";
    $direccion = "Ronda de la Química s/n Edificio AMB L'Andana Planta 7, Paterna";
    $x = "39.543994";
    $y = "-0.4663604";
}
else if($where == 3)
{
    $lugar = "Centre del Carme Cultura Contemporánia (CCCC)";
    $direccion = "Calle del Museo, 2, Valencia";
    $x = "39.4795912";
    $y = "-0.3812556";
}
else
{
    $lugar = "Las Naves";
    $direccion = "Carrer de Joan Verdeguer, 16, Valencia";
    $x = "39.4586801";
    $y = "-0.3401427";
}
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <header class="entry-header">
        <?php
        if ( is_singular() ) :
            echo '<h1 class="entry-title">'.$number.'.- '.get_the_title().'</h1>';
        else :
            echo '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">'.$number.'.- '.get_the_title().'</a></h2>';
        endif;

        if ( 'post' === get_post_type() ) : ?>
        <div class="entry-meta">
            <?php gnulinuxvalencia_posted_on(); ?>
        </div><!-- .entry-meta -->
        <?php
        endif; ?>
    </header><!-- .entry-header -->

    <div class="entry-content">
    <?php
    //if(is_singular() && isset($themes) && strlen($themes) > 0){
        ?>
        <div class="meeting">
            <h3>Convocatoria:</h3>
            <?php echo $meeting;?>
        </div>
        <div class="themes">
            <h3>Temas:</h3>
            <ul>
                <?php
                $themes = explode( ';', trim($themes));
                foreach ($themes as $theme)
                {
                    if(isset($theme) && strlen(trim($theme)) >0)
                    {
                        echo '<li>'.trim($theme).'</li>';
                    }
                }
                ?>
            </ul>
            </p>
        </div>
        <?php
        if(is_singular() && isset($date) && $date->getTimestamp() < date_create()->getTimestamp()){?>
        <div class="chronic">
            <h3>Crónica:</h3>
            <?php
                the_content( sprintf(
                    wp_kses(
                        /* translators: %s: Name of current post. Only visible to screen readers */
                        __( 'Continúa leyendo<span class="screen-reader-text"> "%s"</span>', 'gnulinuxvalencia' ),
                        array(
                            'span' => array(
                                'class' => array(),
                            ),
                        )
                    ),
                    get_the_title()
                ) );

                wp_link_pages( array(
                    'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'gnulinuxvalencia' ),
                    'after'  => '</div>',
                ) );
            ?>
        </div>
    <?php } ?>
    </div><!-- .entry-content -->
    <?php
    if(is_singular()){?>
    <div>
        <p><strong>Lugar:</strong> <a href="https://www.google.com/maps/@<?php echo $x.','.$y.',17z';?>"><?php echo $lugar;?></a></p>
        <p><strong>Dirección:</strong> <?php echo $direccion;?></p>
        <p><strong>Fecha:</strong> <?php echo strftime('%A, %d de %B del %G',  $date->getTimestamp());?></p>
        <p><strong>Horario:</strong> de <?php echo $start;?> a <?php echo $end;?></p>
    </div>

    <div id="map"></div>
    <script type="text/javascript" src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"></script>
    <script>
        var map = L.map('map').setView([<?php echo $x.",".$y;?>], 15);

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);

        L.marker([<?php echo $x.",".$y;?>]).addTo(map)
            .bindPopup("<?php echo $lugar;?>")
            .openPopup();
    </script>
    <?php }?>
    <footer class="entry-footer">
        <?php gnulinuxvalencia_entry_footer(); ?>
    </footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
