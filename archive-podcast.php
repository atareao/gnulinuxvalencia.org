<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package lorenzocarbonell_201711
 */
global $wp_query;
get_header(); ?>
    <div id="primary" class="content-area podcasts">
        <main id="main" class="site-main">

        <?php
        //query_posts( 'posts_per_page=8' );
        //
        //
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $args = array('post_type' => get_post_type(),
                      'posts_per_page' => 5,
                      'orderby' => 'date',
                      'order'=> 'DESC',
                      'paged' => $paged);
        $original_query = $wp_query;
        $wp_query = new WP_Query($args);
        if ( $wp_query->have_posts() ) : ?>

            <header class="page-header">
            </header><!-- .page-header -->

            <?php
            /* Start the Loop */
            $index=0;
            while ( $wp_query->have_posts() ) : $wp_query->the_post();

                /*
                 * Include the Post-Format-specific template for the content.
                 * If you want to override this in a child theme, then include a file
                 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                 */
                if (($wp_query->current_post +1) == ($wp_query->post_count)) {
                    set_query_var( 'is_last_article', 1 );
                }else{
                    set_query_var( 'is_last_article', 0 );
                }
                set_query_var( 'article_index', $index );
                get_template_part( 'template-parts/content', 'podcast' );
                $index++;
            endwhile;
            $wp_query = $original_query;
            echo '<div class="articles">';
            atareao_theme_pagination();
            echo '</div>';

            //the_posts_navigation();

        else :

            get_template_part( 'template-parts/content', 'none' );

        endif;
        
        ?>

        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_footer();
