<?php
/*
 * Template Name: landing sin sidebar
 * Description: Esto es sencillo
 */
get_header(); ?>
<div id="primary" class="content-area alone">
    <main id="main" class="site-main">
    <?php while ( have_posts() ) : the_post(); ?>
        <?php get_template_part( 'template-parts/content', 'page' ); ?>
    <?php endwhile; // end of the loop. ?>
  </main><!-- main -->
</div><!-- #primary -->
<?php get_footer(); ?>