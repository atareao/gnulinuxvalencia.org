<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package atareao_theme_v2
 *
 * Template Name: Contactar
 *
 */
define('__ROOT__', dirname(dirname(__FILE__)));
require_once(__ROOT__.'/lib/recaptchalib.php');

$options = get_option('admin_settings_options');
$secret = $options['admin_settings_secret'];
$key = $options['admin_settings_key'];
$concact_email = $options['admin_settings_concact_email'];

if (!empty($secret)){
    $reCaptcha = new ReCaptcha($secret);
}else{
    $reCaptcha = null;
}
// respuesta vacía
global $response;
$response = '';
//user posted variables
$nombre = ucwords(trim(esc_attr($_POST['message_name'])));
$email = strtolower(trim(esc_attr($_POST['message_email'])));
$subject = $_POST['message_subject'];
$message = $_POST['message_text'];

if(isset($nombre) && !empty($nombre)) {
    $nombre_y_apellidos = explode(' ', $nombre);
    if(count($nombre_y_apellidos) > 1){
        if(count($nombre_y_apellidos) > 3){
            $nombre = $nombre_y_apellidos[0].' '.$nombre_y_apellidos[1];
            unset($nombre_y_apellidos[1]);
            unset($nombre_y_apellidos[0]);
            $apellidos = implode(' ', $nombre_y_apellidos);
        }else{
            $nombre = $nombre_y_apellidos[0];
            unset($nombre_y_apellidos[0]);
            $apellidos = implode(' ', $nombre_y_apellidos);
        }
    }else{
        $apellidos = '';
    }
}else{
    $apellidos = '';
}

//php mailer variables

//$table = 'wp_suscripciones';

//validate email
if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
    if(!empty($email)){
        $response = 'Falta por cumplimentar el email';
    }else{
        if(!empty($nombre)||!empty($message)||!empty($subject)){
            $response = 'El email no es válido';
        }
    }
}else{ //email is valid
    //validate presence of name and message
    if(empty($nombre) || empty($message)){
        if(empty($nombre)){
            $response = 'Falta por cumplimentar el nombre';
        }elseif(empty($message)){
            $response = 'Falta por cumplimentar el mensaje';
        }elseif(empty($nombre) && empty($message)){
            $response = 'Falta por cumplimentar tanto el nombre como el mensaje';
        }
    }else{
        if(filter_var($email, FILTER_VALIDATE_EMAIL) && !empty($nombre) && !empty($message)){
            $ok = false;
            if (empty($secret)) {
                $ok = true;
            } elseif($_POST["g-recaptcha-response"]) {
                $response_reCaptcha = $reCaptcha->verifyResponse(
                        $_SERVER["REMOTE_ADDR"],
                        $_POST["g-recaptcha-response"]
                    );
                if ($response_reCaptcha != null && $response_reCaptcha->success) {
                    $ok = true;
                }
            }
            if ($ok == true){
                global $wpdb;
                $registrado = false;
                if(isset($table))
                {
                    $find_row = $wpdb->get_row($wpdb->prepare('SELECT * FROM '.$table.' WHERE email = %s;', $email));
                    if(!is_null($find_row)){
                        $registrado = true;
                        $subject = $subject . " (Desde gnulinuxvalencia.org - registrado)";
                    }else{
                        $subject = $subject . " (Desde gnulinuxvalencia.org - no registrado)";
                    }
                }
                else
                {
                    $subject = $subject . " (Desde gnulinuxvalencia.org - no registrado)";
                }
                $headers = 'From: '. $email . "\r\n" . 'Reply-To: ' . $email . "\r\n";
                $message = "**Desde gnulinuxvalencia.org**\r\n/================/\r\nAsunto:" . $subject . "\r\nCorreo de: " . $nombre . " \r\n/================/\r\n\r\n" .$message;
                $sent = wp_mail($concact_email, $subject, strip_tags($message), $headers);
                if($sent) {
                    if ($registrado == true) {
                        $response = 'enviado - registrado';
                    } else {
                        $response = 'enviado';
                    }
                } else {
                    $response = 'No se ha podido enviar el mensaje';
                }
            }
        }
    }
}
get_header();
?>
<div id="primary" class="content-area alone">
    <main id="main" class="site-main">
        <article>
            <?php
            if(($response=='enviado')||($response=='enviado - registrado')){
                if($response=='enviado'){
                    ?>
                    <p>Perfecto, <strong><?php echo $nombre;?></strong>, correo enviado.</p>
                    <p>Si quieres, puedes seguir navegando por la web, haciendo clic en el siguiente botón.</p>
                     <form action="https://gnulinuxvalencia.org/" method="post">
                        <div class="u-text-center">
                            <input class="aligncenter" type="submit" value="Navegar" />
                        </div>
                    </form>
                    <?php
                }else{
                    ?>
                    <p>Perfecto, <strong><?php echo $nombre;?></strong>, correo enviado.</p>
                    <p>Como estás suscrito a GNU/Linux Valencia, sabes, que tus correos tienen preferencia. Esperamos dar respuesta a tu correo, cuanto antes.</p>
                    <p>Si quieres, puedes seguir navegando por la web, haciendo clic en el siguiente botón.</p>
                     <form action="https://gnulinuxvalencia.org/" method="post">
                        <div class="u-text-center">
                            <input class="aligncenter" type="submit" value="Navegar" />
                        </div>
                    </form>
                    <?php
                }
            }else{ ?>
                <h3>Contactar</h3>
                <?php if($response != ''){?>
                <div id="result"><div class='error'><?php echo $response;?></div></div>
                <?php } ?>
                <div>
                    <form enctype="application/x-www-form-urlencoded" action="" method="post">
                            <input class="user-icon" type="text" name="message_name" placeholder="Nombre (*)" value="<?php esc_attr($_POST['message_name'])?>"/>
                            <input class="email-icon" type="email" name="message_email" placeholder="Email (*)" value="<?php esc_attr($_POST['message_email'])?>"/>
                            <input class="info-icon" type="text" name="message_subject" placeholder="Asunto" value="<?php esc_attr($_POST['message_subject'])?>"/>
                            <textarea class="message-icon" name="message_text"  placeholder="Mensaje (*)"><?php esc_textarea($_POST['message_text'])?></textarea>
                            <?php
                            if(!empty($secret)){?>
                            <div class="g-recaptcha" data-sitekey="<?php echo $key;?>"></div>
                            <input type="hidden" name="submitted" value="1">
                            <?php }?>
                            <div class="u-text-center">
                                <input class="aligncenter" type="submit" value="Enviar mensaje" />
                            </div>
                    </form>
                    <?php
                    if(!empty($secret)){?>
                    <!--js-->
                    <script src="https://www.google.com/recaptcha/api.js"></script>
                    <?php }?>
                </div>
            <?php }?>
        </article>
    </main><!-- #main -->
</div><!-- #primary -->
<?php
get_footer_without_colophon();
