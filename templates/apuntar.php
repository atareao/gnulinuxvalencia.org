<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package atareao_theme_v2
 *
 * Template Name: suscribir
 *
 */
define('__ROOT__', dirname(__FILE__, 2));
require_once(__ROOT__.'/lib/mailrelay.php');
require_once(__ROOT__.'/lib/recaptchalib.php');
// tu clave secreta
$secret = '6LcFwGIUAAAAAGfBSiWXfuwMguAnprpsADZWPgVX';
// respuesta vacía
// comprueba la clave secreta
if (!empty($secret)){
    $reCaptcha = new ReCaptcha($secret);
}else{
    $reCaptcha = null;
}
$action = '';
// 'Actions:'
// - CREATE: create suscription
// - CONFIRM: confirm suscription
// - REMOVE: remove suscription
$response = '';
// 'Responses:'
// - OK
// - ERROR-EXISTS: 
//function to generate response
function atareao_theme_v2_suscribir_generate_response($type, $message){
    global $response;
    if($type == 'error') {
        $response = "<div class='error'>{$message}</div>";
    }
}
function set_html_mail_content_type() {
    return 'text/html';
}
//response messages
$missing_content = "Por favor introduce la información solicitada.";
$email_invalid   = "Dirección de correo electrónico incorrecta.";

$table = 'wp_suscripciones';
$admin_email = get_option('admin_email');

//user posted variables
global $nombre;
global $email;
$nombre = ucwords(trim(esc_attr($_POST['message_name'])));
$email = strtolower(trim(esc_attr($_POST['message_email'])));

if(isset($nombre) && !empty($nombre)) {
    $nombre_y_apellidos = explode(' ', $nombre);
    if(count($nombre_y_apellidos) > 1){
        if(count($nombre_y_apellidos) > 3){
            $nombre = $nombre_y_apellidos[0].' '.$nombre_y_apellidos[1];
            unset($nombre_y_apellidos[1]);
            unset($nombre_y_apellidos[0]);
            $apellidos = implode(' ', $nombre_y_apellidos);
        }else{
            $nombre = $nombre_y_apellidos[0];
            unset($nombre_y_apellidos[0]);
            $apellidos = implode(' ', $nombre_y_apellidos);
        }
    }else{
        $apellidos = '';
    }
}else{
    $apellidos = '';
}

$date = date('Y-m-d H:i:s');
$action = (esc_attr($_GET['action']));
$token = (esc_attr($_GET['token']));
if($action==''){
    $action = 'CREATE';
}

//echo 'Nombre: '.$nombre.'<br/>';
//echo 'Apellidos: '.$apellidos.'<br/>';
//echo 'Email: '.$email.'<br/>';
//echo 'Fecha: '.$date.'<br/>';
//echo 'Response: '.$response.'<br/>';
//echo 'Action: '.$action.'<br/>';
//echo 'Token: '.$token.'<br/>';

if($action === 'CONFIRM'){
    global $wpdb;
    $find_row = $wpdb->get_row($wpdb->prepare('SELECT * FROM '.$table.' WHERE token = %s;', $token));
    if(!is_null($find_row)){
        $nombre=$find_row->nombre;
        $email=$find_row->email;
        $success=$wpdb->update(
            $table,
            array(
                'alta'          => '1',
                'fecha_alta'    => $date
            ),
            array(
                'id'            => $find_row->id
            ),
            array(
                '%s',
                '%s'
            ),
            array(
                '%s'
            ));
        if($success){
            $address = 'atareao.ip-zone.com';
            $api_key = 'A8Z7eCl5iC7YTgBKPJyujKvkCncyklzlkOjERKA0';
            $url_baja = get_permalink().'?action=REMOVE&token='.$token;
            $ans = add_suscriber($address, $api_key, $nombre, $email, '3', '3', $url_baja);
            if($ans == true){
                $msg_title = 'Suscripción completa';
                $msg=$nombre.' con email '.$email. ' ha completado la suscripción';
            }else{
                $msg_title = 'Suscripción incompleta';
                $msg=$nombre.' con email '.$email. ' ha completado la suscripción, pero NO se ha podido añadir el suscriptor a MailRelay';
            }
            wp_mail( $admin_email, $msg_title, $msg);
        }
    }
}elseif($action === 'REMOVE'){
    $find_row = $wpdb->get_row($wpdb->prepare('SELECT * FROM '.$table.' WHERE token = %s;', $token));
    if(!is_null($find_row)){
        $nombre=$find_row->nombre;
        $email=$find_row->email;
        $success=$wpdb->update(
            $table,
            array(
                'alta'          => '0',
                'fecha_baja'    => $date
            ),
            array(
                'id'            => $find_row->id
            ),
            array(
                '%s',
                '%s'
            ),
            array(
                '%s'
            ));
        if($success){
            $address = 'atareao.ip-zone.com';
            $api_key = 'A8Z7eCl5iC7YTgBKPJyujKvkCncyklzlkOjERKA0';
            $ans = delete_suscriber($address, $api_key, $email);
            if($ans == true){
                $msg_title = 'Baja de el Atareao';
                $msg=$nombre.' con email '.$email. ' se ha dado de BAJA de el Atareao';
            }else{
                $msg_title = 'Baja de el Atareao incompleta';
                $msg=$nombre.' con email '.$email. ' se ha dado de BAJA de el Atareao, pero NO se ha podido quitar de MailRelay';
            }
            wp_mail( $admin_email, $msg_title, $msg);
        }
    }
}else{
    if(empty($email)||empty($nombre)){
        $response='';
    }else{
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            if(!empty($email)){
                atareao_theme_v2_suscribir_generate_response("error", $email_invalid);
            }
        }else{ //email is valid
            //validate presence of name and message
            if(empty($nombre)){
                atareao_theme_v2_suscribir_generate_response("error", $missing_content);
            }else{
                if(filter_var($email, FILTER_VALIDATE_EMAIL) && !empty($nombre)){
                    $ok = false;
                    if(empty($secret)){
                        $ok = true;
                    }elseif($_POST["g-recaptcha-response"]) {
                        $response_reCaptcha = $reCaptcha->verifyResponse(
                                $_SERVER["REMOTE_ADDR"],
                                $_POST["g-recaptcha-response"]
                            );
                        if ($response_reCaptcha != null && $response_reCaptcha->success) {
                            $ok = true;
                        }
                    }
                    if ($ok ==true){
                        global $wpdb;
                        $token = sha1($nombre . $apellidos . $email . $date);
                        $data = array(
                            'nombre'            => $nombre,
                            'apellidos'         => $apellidos,
                            'email'             => $email,
                            'fecha_suscripcion' => $date,
                            'token'             => $token
                        );
                        $format = array(
                            '%s',
                            '%s',
                            '%s',
                            '%s',
                            '%s'
                        );
                        $success=$wpdb->insert( $table, $data, $format );
                        if($success){
                            #echo  atareao_theme_v2_suscribir_generate_response('success',$suscription_ok);
                            $multiple_recipients = array(
                                    $admin_email,
                                    $email
                            );
                            $subj = 'Confirma tu suscripción a El atareao';
                            $url = get_permalink().'?action=CONFIRM&token='.$token;
                            $body = '<p>Hola <strong>'.$nombre.'</strong>,</p><p><a href="'.$url.'">haz clic aquí para completar tu suscripción a <strong>El atareao</strong>.</a></p><p>Si no has solicitado la suscripción a <strong>El atareao</strong>, simplemente ignora este correo y la suscripción no se activará.</p><p>Si tienes alguna pregunta con respecto a este correo electrónico, ponte en contacto conmigo respondiendo a este mismo correo.</p><p>Un saludo.</p>';
                            add_filter( 'wp_mail_content_type', 'set_html_mail_content_type' );
                            wp_mail( $multiple_recipients, $subj, $body );
                            remove_filter( 'wp_mail_content_type', 'set_html_mail_content_type' );
                            $response='por confirmar';
                        }else{
                            $find_row = $wpdb->get_row($wpdb->prepare('SELECT * FROM '.$table.' WHERE email = %s;', $email));
                            if(!is_null($find_row)){
                                if($find_row->alta==0){
                                    $token = $find_row->token;
                                    $nombre = $find_row->nombre;
                                    $email = $find_row->email;
                                    $multiple_recipients = array(
                                            $admin_email,
                                            $email
                                    );
                                    $subj = 'Confirma tu suscripción a El atareao';
                                    $url = get_permalink().'?action=CONFIRM&token='.$token;
                                    $body = '<p>Hola <strong>'.$nombre.'</strong>,</p><p><a href="'.$url.'">haz clic aquí para completar tu suscripción a <strong>El atareao</strong>.</a></p><p>Si no has solicitado la suscripción a <strong>El atareao</strong>, simplemente ignora este correo y la suscripción no se activará.</p><p>Si tienes alguna pregunta con respecto a este correo electrónico, ponte en contacto conmigo respondiendo a este mismo correo.</p><p>Un saludo.</p>';
                                    add_filter( 'wp_mail_content_type', 'set_html_mail_content_type' );
                                    wp_mail( $multiple_recipients, $subj, $body );
                                    remove_filter( 'wp_mail_content_type', 'set_html_mail_content_type' );
                                    $response='por confirmar';
                                }else{
                                    $response='ya existe';
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
get_header(); 
?>
<div id="primary" class="content-area-alone">
    <main id="main" class="site-main">
        <article>
        <?php
            if($action=='CREATE'){
                #echo "Response: ".$response;
                #the_permalink();
                if($response==''){?>
                    <div id="result"><?php echo $response;?></div>
                    <form action="" method="post">
                        <input class="user-icon" type="text" name="message_name" class="field-style field-full align-none" placeholder="Nombre (*)" value="<?php esc_attr($_POST['message_name'])?>"/>
                        <input class="email-icon" type="email" name="message_email" class="field-style field-full align-none" placeholder="Email (*)" value="<?php esc_attr($_POST['message_email'])?>"/>
                        <?php if(!empty($secret)){?>
                        <div class="g-recaptcha" data-sitekey="6LcFwGIUAAAAAMBpBcBWSSjl3n3OiXe3O3tAzhuX"></div>
                        <input type="hidden" name="submitted" value="1">
                        <?php }?>
                        <input class="aligncenter" type="submit" value="Apúntate" />
                    </form>
                    <?php
                    if(!empty($secret)){?>
                    <!--js-->
                    <script src="https://www.google.com/recaptcha/api.js"></script>
                    <?php }?>
        <?php
                }elseif($response=='por confirmar'){
                    ?>
                        <p>Perfecto, <strong><?php echo $nombre;?></strong>, aún queda un paso más.</p>
                        <p>En <strong>breve</strong> recibirás un <strong>email</strong> para confirmar tu suscripción. Por favor, <strong>comprueba</strong> tu correo para activar la cuenta. Si no encuentras el correo de de confirmación, por favor búscalo en la Bandeja de NO DESEADOS o SPAM y agrega a CONTACTOS la dirección de correo del Remitente.</p>
                        <p><strong>Gracias</strong> por tu paciencia y tu interés. Si quieres, puedes seguir navegando por el sitio, haciendo clic en el siguiente botón.</p>
                         <form action="https://www.atareao.es/blog/" method="post">
                            <div class="u-text-center">
                                <input class="minimal-button" type="submit" value="Navegar" />
                            </div>
                        </form>
                    <?php
                }
                elseif($response=='ya existe'){
                    ?>
                        <p><strong><?php echo $nombre;?></strong>, para mi es un placer informarte que <strong>ya te habías suscrito anteriormente</strong>. No necesitas hacer nada mas.<p>
                        <p>De cualquier forma, te doy las <strong>gracias</strong> por tu confianza. Si quieres, puedes seguir navegando por el sitio, haciendo clic en el siguiente botón.</p>
                         <form action="https://www.atareao.es/blog/" method="post">
                            <div class="u-text-center">
                                <input class="minimal-button" type="submit" value="Navegar" />
                            </div>
                        </form>
                    <?php
                }
            }elseif($action==='CONFIRM'){
                global $nombre;?>
                <p>Muchas gracias por suscribirte, <strong><?php echo $nombre;?></strong>.</p>
                <p>A partir de ahora, y con una frecuencia <strong>semanal</strong>, recibirás una <strong>actualización de los artículos publicados</strong>, novedades, así como nuevas aplicaciones y mejoras. En estos artículos encontrarás una guía para tener, sin lugar a dudas, el <strong>mejor sistema operativo instalado</strong> en tu equipo.</p>
                <p>Ahora que estás suscrito, siempre que me mandes un correo mediante el <strong>formulario de contacto</strong>, para resolver alguna duda, comentario o por lo que necesites, tendrás trato preferente.</p>
                <p>Por otro lado, estoy preparando nuevos tutoriales, cursos y aplicaciones. Te iré informando puntualmente de estas novedades, para que seas el primero en saber de ellas.</p>
                <p>Por último, estoy escribiendo un <strong>libro</strong> sobre <strong>Ubuntu</strong>. En cuanto tenga terminado el libro <strong>serás el primero en recibirlo</strong>.</p>
                <p>Muchas <strong>gracias</strong> y sigue disfrutando de este <strong>tu</strong> sitio. Si quieres, puedes seguir navegando por el sitio, haciendo clic en el siguiente botón.</p>
                 <form action="https://www.atareao.es/blog/" method="post">
                    <div class="u-text-center">
                        <input class="minimal-button" type="submit" value="Navegar" />
                    </div>
                </form>
        <?php
            }elseif($action=='REMOVE'){
                global $nombre;?>
                <p><strong><?php echo $nombre;?></strong>, lamento que te des de baja.</p>
                <p>A partir de ahora, dejarás de recibir el correo semanal y las informaciones puntuales.</p>
                <p>De cualquier forma, muchas <strong>gracias</strong> y sigue disfrutando de este <strong>tu</strong> sitio. Si quieres, puedes seguir navegando por el sitio, haciendo clic en el siguiente botón.</p>
                 <form action="https://www.atareao.es/blog/" method="post">
                    <div class="u-text-center">
                        <input class="minimal-button" type="submit" value="Navegar" />
                    </div>
                </form>
        <?php
            }
        ?>
        </article>
    </main><!-- #main -->
</div><!-- #primary -->
<?php
get_footer_without_colophon();
