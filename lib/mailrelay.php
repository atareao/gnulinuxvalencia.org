<?php
/**
 * Librería PHP para trabajar con MailRelay
 *  - Documentación: https://mailrelay.com/es/api-documentation/
 *
 * @copyright Copyright (c) 2017, Lorenzo Carbonell.
 * @link      https://www.atareao.es
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

    /**
     * Add suscriber to MailRelay
     *
     * @param string $address the address of your email list.
     * @param string $api_key the API key for the account
     * @param string $name the user name
     * @param string $email the user email
     * @param string $group the group to suscribe the user
     * @param array $custom_fields campos personales
     * @return array response
     */

function add_suscriber($address, $api_key, $name, $email, $group, $field_id, $url_baja){
    $url = 'https://'.$address.'/ccm/admin/api/version/2/&type=json';
    $curl = curl_init($url);
    $postData = array(
        'function' => 'addSubscriber',
        'apiKey' => $api_key,
        'email' => $email,
        'name' => $name,
        'groups' => array(
            $group
        ),
        'customFields' => array(
            'f_'.$field_id => $url_baja
        )
    );

    $post = http_build_query($postData);

    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

    $json = curl_exec($curl);
    if ($json === false) {
        # die('Request failed with error: '. curl_error($curl));
        return false;
    }
 
    $result = json_decode($json);
    if ($result->status == 0) {
        # die('Bad status returned. Error: '. $result->error);
        return false;
    }
    # var_dump($result->data);
    return true;
}

    /**
     * Remove suscriber from MailRelay
     *
     * @param string $address the address of your email list.
     * @param string $api_key the API key for the account
     * @param string $email the user email
     */

function delete_suscriber($address, $api_key, $email){
    $url = 'https://'.$address.'/ccm/admin/api/version/2/&type=json';
    $curl = curl_init($url);
    $postData = array(
        'function' => 'deleteSubscriber',
        'apiKey' => $api_key,
        'email' => $email
    );

    $post = http_build_query($postData);

    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

    $json = curl_exec($curl);
    if ($json === false) {
        # die('Request failed with error: '. curl_error($curl));
        return false;
    }
 
    $result = json_decode($json);
    if ($result->status == 0) {
        # die('Bad status returned. Error: '. $result->error);
        return false;
    }
    # var_dump($result->data);
    return true;
}