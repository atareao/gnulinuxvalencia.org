<?php
$default_image = get_template_directory_uri().'/images/podcast-gnu-linux-valencia.jpg';

$args = array('post_type'       => array('evento', 'podcast'),
              'posts_per_page' => 10);
$loop = new WP_Query( $args );
header('Content-Type: '.feed_content_type('rss-http').'; charset='.get_option('blog_charset'), true);
echo '<?xml version="1.0" encoding="'.get_option('blog_charset').'"?'.'>';
?>

<rss version="2.0"
    xmlns:content="http://purl.org/rss/1.0/modules/content/"
    xmlns:wfw="http://wellformedweb.org/CommentAPI/"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:atom="http://www.w3.org/2005/Atom"
    xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
    xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
    xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd"
    xmlns:rawvoice="http://www.rawvoice.com/rawvoiceRssModule/"
    xmlns:googleplay="http://www.google.com/schemas/play-podcasts/1.0"
    >
    <channel>
        <title>GNU/Linux Valencia</title>
        <atom:link href="https://gnulinuxvalencia.org/todo-el-feed/" rel="self" type="application/rss+xml" />
        <link>https://gnulinuxvalencia.org</link>
        <description>Todo lo que siempre has querido saber sobre GNU/Linux en Valencia</description>
        <lastBuildDate><?php echo date('D, d M Y H:i:s O');?></lastBuildDate>
        <language>es</language>
        <sy:updatePeriod>daily</sy:updatePeriod>
        <sy:updateFrequency>1</sy:updateFrequency>
        <generator>https://wordpress.org/?v=4.9.7</generator>
        <itunes:summary>![CDATA[El objeto de este grupo es la difusión de GNU/Linux, para lo cual se pretende realizar reuniones mensuales, para realizar desde instalaciones de distribuciones GNU/Linux en equipos portátiles o de sobremesa, resolver problemas en el uso diario, y dar a conocer la facilidad de uso y funcionamiento de esta distribución de GNU/Linux. Dado el planteamiento del grupo, cualquiera puede unirse, desde usuarios noveles, sin apenas conocimientos informáticos, hasta usuarios avanzados, que pueden o bien ayudar a los recién llegados resolviendo sus dudas o problemáticas, o incluso el desarrollo de aplicaciones informáticas para la resolución de problemas cotidianos. La razón de unirse a este grupo, es la de conocer una solución alternativa a los sistemas operativos privativos y descubrir la facilidad de su uso para aquellos usuarios noveles, y en cuanto a los usuarios mas avanzados, compartir experiencias y descubrir novedades.]]</itunes:summary>
        <itunes:author>GNU/Linux Valencia</itunes:author>
        <itunes:explicit>clean</itunes:explicit>
        <itunes:image href="<?php echo $default_image?>" />
        <itunes:owner>
            <itunes:name>GNU/Linux Valencia</itunes:name>
            <itunes:email>correo@gnulinuxvalencia.org</itunes:email>
        </itunes:owner>
        <managingEditor>correo@gnulinuxvalencia.org (GNU/Linux Valencia)</managingEditor>
        <itunes:subtitle>Todo lo que siempre has querido saber sobre GNU/Linux en Valencia</itunes:subtitle>
        <copyright><?php echo date('Y'); ?> GNU/Linux Valencia</copyright>
        <itunes:category text="Technology">
            <itunes:category text="Tech News"/>
        </itunes:category>
        <?php  while ( $loop->have_posts() ) : $loop->the_post(); // Start the loop for Podcast posts
            $post_type = get_post_type();
            if($post_type == 'podcast')
            {
                $postID = get_the_ID();
                $description = get_post_meta($postID, 'post_seo_description', true);
                $season = get_post_meta($postID, 'season', true);
                $number = get_post_meta($postID, 'number', true);
                $duration = get_post_meta($postID, 'duration', true);
                $filesize = intval(get_post_meta($postID, 'filesize', true))*1024*1024;
                $ogg_url = get_post_meta($postID, 'ogg-url', true);
                $mp3_url = get_post_meta($postID, 'mp3-url', true);
                $nepisodio = 'T'.substr('00'.$season, -2).'E'.substr('00'.$number, -2);

                $post = get_post($postID); 
                $content = wp_strip_all_tags(apply_filters('the_content', $post->post_content)); 

                $image = get_imagen_destacada();
                if(!isset($image))
                {
                    $image = $default_image;
                }
                ?>

            <item>
                <title><?php echo $nepisodio.' - '.get_the_title();?></title>
                <link><?php the_permalink();?></link>
                <pubDate><?php echo get_the_date('D, d M Y H:i:s O');?></pubDate>
                <guid isPermaLink="false">https://gnulinuxvalencia.org/?post_type=podcast%26p=<?php echo $postID;?></guid>
                <content:encoded><![CDATA[<?php echo $content; ?>]]></content:encoded>
                <description><?php echo $description;?></description>
                <itunes:subtitle><?php echo $description;?></itunes:subtitle>
                <itunes:summary><?php echo $description;?></itunes:summary>
                <enclosure url="<?php echo $ogg_url; ?>" length="<?php echo $filesize; ?>" type="audio/ogg" />
                <enclosure url="<?php echo $mp3_url; ?>" length="<?php echo $filesize; ?>" type="audio/mpeg" />
                <itunes:image href="<?php echo $image;?>" />
                <itunes:author>GNU/Linux Valencia</itunes:author>
                <itunes:explicit>clean</itunes:explicit>
                <itunes:duration><?php echo substr($duration, -2).':00'; ?></itunes:duration>
            </item>
            <?php
        }
        else if($post_type == 'evento')
        {
            $postID = get_the_ID();
            $description = get_post_meta($postID, 'post_seo_description', true);
            $number = get_post_meta($postID, 'number', true);
            $meeting = get_post_meta($postID, 'meeting', true);
            $themes = get_post_meta($postID, 'themes', true);
            $where = get_post_meta($postID, 'where', true);
            $date = new DateTime(get_post_meta($postID, 'date', true));
            $start = get_post_meta($postID, 'start', true);
            $end = get_post_meta($postID, 'end', true);
            if($where == 2)
            {
                $lugar = "Linux Center Valencia";
                $direccion = "Ronda de la Química s/n Edificio AMB L'Andana Planta 7, Paterna";
            }
            else if($where == 3)
            {
                $lugar = "Centre del Carme Cultura Contemporánia (CCCC)";
                $direccion = "Calle del Museo, 2, Valencia";
            }
            else
            {
                $lugar = "Las Naves";
                $direccion = "Carrer de Joan Verdeguer, 16, Valencia";
            }
            $post = get_post($postID); 
            $content = wp_strip_all_tags(apply_filters('the_content', $post->post_content)); 
            $image = get_imagen_destacada();
            if(!isset($image))
            {
                $image = $default_image;
            }
            $title = get_the_title();
            $fecha = strftime('%A, %d de %B del %G',  $date->getTimestamp());
            $content = <<<EOT
<strong>$number - $title</strong>

<strong>Lugar:</strong> $lugar
<strong>Dirección:</strong> $direccion
<strong>Fecha:</strong> $fecha
<strong>Horario:</strong> de $start a $end

<strong>Convocatoria:</strong>
$meeting

<strong>Temas:</strong>
$themes

<strong>Crónica:</strong>
$content
EOT;
            ?>
        <item>
            <title><?php echo $number.' - '.get_the_title();?></title>
            <link><?php the_permalink();?></link>
            <pubDate><?php echo get_the_date('D, d M Y H:i:s O');?></pubDate>
            <guid isPermaLink="false">https://gnulinuxvalencia.org/?post_type=podcast%26p=<?php echo $postID;?></guid>
            <content:encoded><![CDATA[<?php echo $content; ?>]]></content:encoded>
            <description><?php echo $description;?></description>
            <itunes:subtitle><?php echo $description;?></itunes:subtitle>
            <itunes:summary><?php echo $description;?></itunes:summary>
            <itunes:image href="<?php echo $image;?>" />
            <itunes:author>GNU/Linux Valencia</itunes:author>
            <itunes:explicit>clean</itunes:explicit>
        </item>
        <?php
        }
         endwhile; ?>
    </channel>
</rss>