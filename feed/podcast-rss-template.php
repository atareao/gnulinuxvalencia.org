<?php
$default_image = get_template_directory_uri().'/images/podcast-gnu-linux-valencia.jpg';

$args = array('post_type'       => 'podcast',
              'posts_per_page' => -1 );
$loop = new WP_Query( $args );
//header('Content-Type: '.feed_content_type('rss-http').'; charset='.get_option('blog_charset'), true);
//echo '<?xml version="1.0" encoding="'.get_option('blog_charset').'"?'.'>';
echo '<?xml version="1.0" encoding="UTF-8"?>';
echo '<?xml-stylesheet type="text/xsl" media="screen" href="http://feeds.feedburner.com/~d/styles/rss2enclosuresfull.xsl"?>';
echo '<?xml-stylesheet type="text/css" media="screen" href="http://feeds.feedburner.com/~d/styles/itemcontent.css"?>';

?>
<rss xmlns:content="http://purl.org/rss/1.0/modules/content/" 
     xmlns:wfw="http://wellformedweb.org/CommentAPI/" 
     xmlns:dc="http://purl.org/dc/elements/1.1/"
     xmlns:atom="http://www.w3.org/2005/Atom"
     xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
     xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
     xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd"
     xmlns:rawvoice="http://www.rawvoice.com/rawvoiceRssModule/"
     xmlns:googleplay="http://www.google.com/schemas/play-podcasts/1.0"
     xmlns:media="http://search.yahoo.com/mrss/"
     xmlns:feedburner="http://rssnamespace.org/feedburner/ext/1.0" version="2.0">
    >
    <channel>
        <itunes:author>GNU/Linux Valencia</itunes:author>
        <title>GNU/Linux Valencia</title>
        <link>https://gnulinuxvalencia.org</link>
        <description>Todo lo que siempre has querido saber sobre GNU/Linux en Valencia</description>
        <language>es-ES</language>
        <generator>https://wordpress.org/?v=<?php echo get_bloginfo('version');?></generator>
        <image>
            <url><?php echo $default_image;?></url>
            <title>GNU/Linux Valencia</title>
            <link>https://gnulinuxvalencia.org</link>
        </image>
        <itunes:image href="<?php echo $default_image?>" />
        <itunes:explicit>no</itunes:explicit>
        <itunes:type>episodic</itunes:type>
        <lastBuildDate><?php echo date('r');?></lastBuildDate>
        <pubDate><?php echo date('r');?></pubDate>
        <itunes:subtitle>Todo lo que siempre has querido saber sobre GNU/Linux en Valencia</itunes:subtitle>
        <itunes:summary>El objeto de este grupo es la difusión de GNU/Linux, para lo cual se pretende realizar reuniones mensuales, para realizar desde instalaciones de distribuciones GNU/Linux en equipos portátiles o de sobremesa, resolver problemas en el uso diario, y dar a conocer la facilidad de uso y funcionamiento de esta distribución de GNU/Linux. Dado el planteamiento del grupo, cualquiera puede unirse, desde usuarios noveles, sin apenas conocimientos informáticos, hasta usuarios avanzados, que pueden o bien ayudar a los recién llegados resolviendo sus dudas o problemáticas, o incluso el desarrollo de aplicaciones informáticas para la resolución de problemas cotidianos. La razón de unirse a este grupo, es la de conocer una solución alternativa a los sistemas operativos privativos y descubrir la facilidad de su uso para aquellos usuarios noveles, y en cuanto a los usuarios mas avanzados, compartir experiencias y descubrir novedades.</itunes:summary>
        <copyright><?php echo date('Y'); ?> GNU/Linux Valencia</copyright>
        <feedburner:info uri="gnulinuxvalenciapodcast" />
        <media:copyright>GNU/Linux Valencia</media:copyright>
        <media:thumbnail url="<?php echo $default_image?>" />
        <media:keywords>GNU,Linux,GNU/Linux,Software,Libre,Open,Source,código,libre,Ubuntu,Fedora,Manjaro,Arch,Elementary,Linux,Mint,GNOME,KDE,Plasma,Cinnamon,XFCE</media:keywords>
        <media:category scheme="http://www.itunes.com/dtds/podcast-1.0.dtd">Technology/Software How-To</media:category>
        <itunes:keywords>GNU,Linux,GNU/Linux,Software,Libre,Open,Source,código,libre,Ubuntu,Fedora,Manjaro,Arch,Elementary,Linux,Mint,GNOME,KDE,Plasma,Cinnamon,XFCE</itunes:keywords>
        <sy:updatePeriod>daily</sy:updatePeriod>
        <sy:updateFrequency>1</sy:updateFrequency>
        <atom10:link xmlns:atom10="http://www.w3.org/2005/Atom" rel="self" type="application/rss+xml" href="http://feeds.feedburner.com/GNULinuxValenciaPodcast" />
        <feedburner:info uri="gnulinuxvalenciapodcast" />
        <atom10:link xmlns:atom10="http://www.w3.org/2005/Atom" rel="hub" href="http://pubsubhubbub.appspot.com/" />
        <media:category scheme="http://www.itunes.com/dtds/podcast-1.0.dtd">Technology/Software How-To</media:category>
        <itunes:owner>
            <itunes:email>correo@gnulinuxvalencia.org</itunes:email>
            <itunes:name>GNU/Linux Valencia</itunes:name>
        </itunes:owner>
        <itunes:category text="Technology">
            <itunes:category text="Software How-To" />
        </itunes:category>

        <?php  while ( $loop->have_posts() ) : $loop->the_post(); // Start the loop for Podcast posts
            $postID = get_the_ID();
            $description = get_post_meta($postID, 'post_seo_description', true);
            $season = get_post_meta($postID, 'season', true);
            $number = get_post_meta($postID, 'number', true);
            $duration = get_post_meta($postID, 'duration', true);
            $filesize = intval(get_post_meta($postID, 'filesize', true))*1024*1024;
            $ogg_url = get_post_meta($postID, 'ogg-url', true);
            $mp3_url = get_post_meta($postID, 'mp3-url', true);
            $nepisodio = 'T'.substr('00'.$season, -2).'E'.substr('00'.$number, -2);

            $post = get_post($postID); 
            $content = wp_strip_all_tags(apply_filters('the_content', $post->post_content)); 

            $image = get_imagen_destacada();
            if(!isset($image))
            {
                $image = $default_image;
            }
            ?>
        <item>
            <title><?php echo $nepisodio.' - '.get_the_title();?></title>
            <link><?php the_permalink();?></link>
            <enclosure url="<?php echo $ogg_url; ?>" length="<?php echo $filesize; ?>" type="audio/ogg" />
            <enclosure url="<?php echo $mp3_url; ?>" length="<?php echo $filesize; ?>" type="audio/mpeg" />
            <description><?php echo $description;?></description>
            <pubDate><?php echo the_time('r');?></pubDate>
            <itunes:duration><?php echo substr($duration, -2).':00'; ?></itunes:duration>
            <itunes:episodeType>full</itunes:episodeType>
            <itunes:season><?php echo $season;?></itunes:season>
            <itunes:episode><?php echo $number;?></itunes:episode>
            <guid isPermaLink="false">https://gnulinuxvalencia.org/?post_type=podcast%26p=<?php echo $postID;?></guid>
            <image>
                <url><?php echo $image;?></url>
                <title><?php echo $nepisodio.' - '.get_the_title();?></title>
                <link><?php the_permalink();?></link>
            </image>
            <itunes:image href="<?php echo $image;?>" />
            <itunes:explicit>No</itunes:explicit>
            <content:encoded><![CDATA[<?php echo $content; ?>]]></content:encoded>
            <itunes:summary><?php echo $description;?></itunes:summary>
            <itunes:subtitle><?php echo substr($description, 0, 255);?></itunes:subtitle>
            <itunes:author>GNU/Linux Valencia</itunes:author>
        </item>
        <?php endwhile; ?>
    </channel>
</rss>