<?php

add_action('init', 'custom_feeds');
function custom_feeds()
{
    add_feed('eventos-feed', 'custom_eventos_feed');
    add_feed('podcasts-feed', 'custom_podcasts_feed');
    add_feed('podcasts-mp3-feed', 'custom_podcasts_mp3_feed');
    add_feed('todo-el-feed', 'custom_all_feed');
}

function custom_podcasts_feed()
{
    require(get_template_directory().'/feed/podcast-rss-template.php' );
}
function custom_podcasts_mp3_feed()
{
    require(get_template_directory().'/feed/podcasts-mp3-rss-template.php' );
}

function custom_eventos_feed()
{
    require(get_template_directory().'/feed/evento-rss-template.php' );
}

function custom_all_feed()
{
    require(get_template_directory().'/feed/general-rss-template.php' );
}