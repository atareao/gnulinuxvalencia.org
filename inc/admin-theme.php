<?php
/**
 * @internal never define functions inside callbacks.
 * these functions could be run multiple times; this would result in a fatal error.
 */
 
/**
 * custom option and settings
 */
add_action( 'admin_init', 'admin_settings_init' );
function admin_settings_init() {
    // register a new setting for "admin_settings" page
    register_setting( 'admin_settings', 'admin_settings_options' );

    // register a new section in the "admin_settings" page
    add_settings_section(
        'admin_settings_section_recaptcha',
        'Opciones de Recaptcha',
        'admin_settings_section_recaptcha_cb',
        'admin_settings'
    );

 // register a new field in the "admin_settings_section_recaptcha" section, inside the "admin_settings" page
    add_settings_field(
        'admin_settings_secret', // as of WP 4.6 this value is used only internally
        // use $args' label_for to populate the id inside the callback
        'Secreto',
        'admin_settings_secret_cb',
        'admin_settings',
        'admin_settings_section_recaptcha',
        [
            'label_for' => 'admin_settings_secret',
            'class' => 'admin_settings_row',
            'admin_settings_custom_data' => 'custom',
        ]
    );
    add_settings_field(
        'admin_settings_key', // as of WP 4.6 this value is used only internally
        // use $args' label_for to populate the id inside the callback
        'Clave',
        'admin_settings_key_cb',
        'admin_settings',
        'admin_settings_section_recaptcha',
        [
            'label_for' => 'admin_settings_key',
            'class' => 'admin_settings_row',
            'admin_settings_custom_data' => 'custom',
        ]
    );

    add_settings_section(
        'admin_settings_section_email',
        'Opciones correo electrónico',
        'admin_settings_section_email_cb',
        'admin_settings'
    );

    add_settings_field(
        'admin_settings_concact_email',
        'Correo de contacto',
        'admin_settings_concact_email_cb',
        'admin_settings',
        'admin_settings_section_email',
        [
            'label_for' => 'admin_settings_concact_email',
            'class' => 'admin_settings_row',
            'admin_settings_custom_data' => 'custom',
        ]
    );


}
 

 
/**
 * custom option and settings:
 * callback functions
 */
 
// developers section cb
 
// section callbacks can accept an $args parameter, which is an array.
// $args have the following keys defined: title, id, callback.
// the values are defined at the add_settings_section() function.
function admin_settings_section_recaptcha_cb( $args ) {
 ?>
 <p id="<?php echo esc_attr( $args['id'] ); ?>"><?php esc_html_e( 'Datos de Recaptcha.', 'admin_settings' ); ?></p>
 <?php
}
 
function admin_settings_section_email_cb( $args ) {
 ?>
 <p id="<?php echo esc_attr( $args['id'] ); ?>"><?php esc_html_e( 'Configuración de correo.', 'admin_settings' ); ?></p>
 <?php
}

function admin_settings_secret_cb($args){
    $options = get_option( 'admin_settings_options' );
    ?>
    <input id="<?php echo esc_attr( $args['label_for'] ); ?>"
    data-custom="<?php echo esc_attr( $args['admin_settings_custom_data'] ); ?>"
    name="admin_settings_options[<?php echo esc_attr( $args['label_for'] ); ?>]"
    value="<?php echo esc_attr($options[ $args['label_for'] ])?>"
    placeholder="<?php echo esc_attr($args['label_for']);?>"
    >
    <?php
}

function admin_settings_key_cb($args){
    $options = get_option( 'admin_settings_options' );
    ?>
    <input id="<?php echo esc_attr( $args['label_for'] ); ?>"
    data-custom="<?php echo esc_attr( $args['admin_settings_custom_data'] ); ?>"
    name="admin_settings_options[<?php echo esc_attr( $args['label_for'] ); ?>]"
    value="<?php echo esc_attr($options[ $args['label_for'] ])?>"
    placeholder="<?php echo esc_attr($args['label_for']);?>"
    >
    <?php
}
 
function admin_settings_concact_email_cb($args){
    $options = get_option( 'admin_settings_options' );
    ?>
    <input id="<?php echo esc_attr( $args['label_for'] ); ?>"
    data-custom="<?php echo esc_attr( $args['admin_settings_custom_data'] ); ?>"
    name="admin_settings_options[<?php echo esc_attr( $args['label_for'] ); ?>]"
    value="<?php echo esc_attr($options[ $args['label_for'] ])?>"
    placeholder="<?php echo esc_attr($args['label_for']);?>"
    >
    <?php
}

/**
 * top level menu
 */
function admin_settings_options_page() {
    // add top level menu page
    add_menu_page(
        'admin_settings',
        'Configuración',
        'manage_options',
        'admin_settings',
        'admin_settings_options_page_html'
    );
}
 
/**
 * register our admin_settings_options_page to the admin_menu action hook
 */
add_action( 'admin_menu', 'admin_settings_options_page' );
 
/**
 * top level menu:
 * callback functions
 */
function admin_settings_options_page_html() {
 // check user capabilities
 if ( ! current_user_can( 'manage_options' ) ) {
 return;
 }
 
 // add error/update messages
 
 // check if the user have submitted the settings
 // wordpress will add the "settings-updated" $_GET parameter to the url
 if ( isset( $_GET['settings-updated'] ) ) {
 // add settings saved message with the class of "updated"
 add_settings_error( 'admin_settings_messages', 'admin_settings_message', __( 'Settings Saved', 'admin_settings' ), 'updated' );
 }
 
 // show error/update messages
 settings_errors( 'admin_settings_messages' );
 ?>
 <div class="wrap">
 <h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
 <form action="options.php" method="post">
 <?php
 // output security fields for the registered setting "admin_settings"
 settings_fields( 'admin_settings' );
 // output setting sections and their fields
 // (sections are registered for "admin_settings", each field is registered to a specific section)
 do_settings_sections( 'admin_settings' );
 // output save settings button
 submit_button( 'Save Settings' );
 ?>
 </form>
 </div>
 <?php
}