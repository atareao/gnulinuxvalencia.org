<?php

/*
CPTs:
    - Trabajo
    - Solución
    - App
*/
add_action('init', 'crear_y_registrar_cpts');
function crear_y_registrar_cpts(){
    // Podcast
    register_post_type('podcast', array(
        'label'         => __('Podcast'),
        'labels'        => array(
                'name'              => __('Podcasts'),
                'singular_name'     => __('Podcast'),
                'add_new'           => __('Nuevo podcast'),
                'add_new_item'      => __('Nuevo podcast'),
                'edit_item'         => __('Editar podcast'),
                'new_item'          => __('Nuevo podcast'),
                'view_item'         => __('Ver podcast'),
                'view_items'        => __('Ver podcasts'),
                'all_items'         => __('Listar podcasts'),
                'menu_name'         => __('Podcasts'),
                'name_admin_bar'    => __('Podcasts')),
        'hierarchical'  => false,
        'public'        => true,
        'has_archive'   => 'podcasts',
        'menu_icon'     => 'dashicons-microphone',
        'show_in_rest'          => true,
        'rest_base'             => 'podcasts',
        'rest_controller_class' => 'WP_REST_Posts_Controller',
        'supports'      => array('editor', 'title', 'author', 'thumbnail', 'comments'),
        'taxonomies'    => array()));
    // Evento
    register_post_type('evento', array(
        'label'         => __('Evento'),
        'labels'        => array(
                'name'              => __('Eventos'),
                'singular_name'     => __('Evento'),
                'add_new'           => __('Nuevo evento'),
                'add_new_item'      => __('Nuevo evento'),
                'edit_item'         => __('Editar evento'),
                'new_item'          => __('Nuevo evento'),
                'view_item'         => __('Ver evento'),
                'view_items'        => __('Ver eventos'),
                'all_items'         => __('Listar eventos'),
                'menu_name'         => __('Eventos'),
                'name_admin_bar'    => __('Eventos')),
        'hierarchical'  => false,
        'public'        => true,
        'has_archive'   => 'eventos',
        'menu_icon'     => 'dashicons-megaphone',
        'show_in_rest'          => true,
        'rest_base'             => 'eventos',
        'rest_controller_class' => 'WP_REST_Posts_Controller',
        'supports'      => array('editor', 'title', 'author', 'thumbnail', 'comments'),
        'taxonomies'    => array()));
}

// Metaboxes para introducir información en los cpts
function podcast_meta_box_markup($object){
    wp_nonce_field(basename(__FILE__), "podcast");
    $season = get_post_meta($object->ID, "season", true);
    if(!isset($season) || strlen($season) < 1)
    {
        $season = date("Y") - 2017;
    }
    $number = get_post_meta($object->ID, "number", true);
    if(!isset($number) || strlen($number) < 1)
    {
        $number = get_max_of_meta_values('number', 'podcast') + 1;
    }
    ?>
        <div>
            <label for="season">Temporada</label>
            <br>
            <input name="season" type="number" value="<?php echo $season; ?>">
            <br>

            <label for="number">Número del episodio</label>
            <br>
            <input name="number" type="number" value="<?php echo $number; ?>">
            <br>

            <label for="duration">Duración</label>
            <br>
            <input name="duration" type="time" value="<?php echo get_post_meta($object->ID, "duration", true); ?>">
            <br>

            <label for="filesize">Tamaño</label>
            <br>
            <input name="filesize" type="number" value="<?php echo get_post_meta($object->ID, "filesize", true); ?>">
            <br>

            <label for="url">Url para ogg</label>
            <br>
            <input name="ogg-url" type="url" value="<?php echo get_post_meta($object->ID, "ogg-url", true); ?>">
            <br>

            <label for="url">Url para mp3</label>
            <br>
            <input name="mp3-url" type="url" value="<?php echo get_post_meta($object->ID, "mp3-url", true); ?>">
            <br>
        </div>
    <?php
} 
function evento_meta_box_markup($object){
    wp_nonce_field(basename(__FILE__), "evento");
    $number = get_post_meta($object->ID, "number", true);
    if(!isset($number) || strlen($number) < 1)
    {
        $number = get_max_of_meta_values('number', 'podcast') + 1;
    }
    ?>
        <div>
            <label for="number">Número de evento</label>
            <br>
            <input name="number" type="number" value="<?php echo $number; ?>">
            <br>

            <label for="meeting">Convocatoria</label>
            <br>
            <textarea name="meeting" rows="10" cols="25"><?php echo get_post_meta($object->ID, "meeting", true); ?></textarea> 
            <br

            <label for="themes">Charlas (separadas por punto y coma)</label>
            <br>
            <textarea name="themes" rows="10" cols="25"><?php echo get_post_meta($object->ID, "themes", true); ?></textarea> 
            <br>

            <label for="where">Donde</label>
            <br>
            <select name="where">
                <option value="1" <?php if ( get_post_meta($object->ID, "where", true) == 1 ) echo 'selected="selected"'; ?>>Las Naves</option>
                <option value="2" <?php if ( get_post_meta($object->ID, "where", true) == 2 ) echo 'selected="selected"'; ?>>Linux Center Valencia</option>
                <option value="3" <?php if ( get_post_meta($object->ID, "where", true) == 3 ) echo 'selected="selected"'; ?>>Centre del Carme Cultura Contemporánia (CCCC)</option>
            </select> 
            <br>

            <label for="date">Fecha</label>
            <br>
            <input name="date" type="date" value="<?php echo get_post_meta($object->ID, "date", true); ?>">
            <br>

            <strong>Horario:</strong>

            <label for="start">Hora de inicio</label>
            <br>
            <input name="start" type="time" value="<?php echo get_post_meta($object->ID, "start", true); ?>">
            <br>

            <label for="end">Hora de fin</label>
            <br>
            <input name="end" type="time" value="<?php echo get_post_meta($object->ID, "end", true); ?>">
            <br>
        </div>
    <?php
}

add_action('add_meta_boxes', 'cpts_custom_meta_box');
function cpts_custom_meta_box()
{
    add_meta_box('podcast_mb', 'Podcast', 'podcast_meta_box_markup', 'podcast', 'side', 'high', null);
    add_meta_box('evento_mb', 'Evento', 'evento_meta_box_markup', 'evento', 'side', 'high', null);
}

function update_post_meta_helper($post_id, $element)
{
    if(isset($_POST[$element])){
        $value = $_POST[$element];
    }else{
        $value = "";
    }
    update_post_meta($post_id, $element, $value);
}

// Callback to Save Meta Data
add_action('save_post', 'podcast_save_custom_meta_box', 10, 3);
function podcast_save_custom_meta_box($post_id, $post, $update)
{
    if(!isset($_POST['podcast']) || !wp_verify_nonce($_POST['podcast'], basename(__FILE__)))
        return $post_id;
    if(!current_user_can("edit_post", $post_id))
        return $post_id;
    if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $post_id;

    $slug = "podcast";
    if($slug != $post->post_type)
        return;

    update_post_meta_helper($post_id, 'season');
    update_post_meta_helper($post_id, 'number');
    update_post_meta_helper($post_id, 'duration');
    update_post_meta_helper($post_id, 'filesize');
    update_post_meta_helper($post_id, 'ogg-url');
    update_post_meta_helper($post_id, 'mp3-url');
}
add_action('save_post', 'evento_save_custom_meta_box', 10, 3);
function evento_save_custom_meta_box($post_id, $post, $update){
    if(!isset($_POST['evento']) || !wp_verify_nonce($_POST['evento'], basename(__FILE__)))
        return $post_id;
    if(!current_user_can("edit_post", $post_id))
        return $post_id;
    if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $post_id;

    $slug = "evento";
    if($slug != $post->post_type)
        return;
    update_post_meta_helper($post_id, 'number');
    update_post_meta_helper($post_id, 'meeting');
    update_post_meta_helper($post_id, 'themes');
    update_post_meta_helper($post_id, 'where');
    update_post_meta_helper($post_id, 'date');
    update_post_meta_helper($post_id, 'start');
    update_post_meta_helper($post_id, 'end');
}

/*
add_filter( 'manage_posts_columns', 'modify_post_table' );
function modify_post_table( $column ) {
    $column['post_type'] = 'Tipo de post';
    return $column;
}

add_filter( 'manage_posts_custom_column', 'modify_post_table_row', 10, 2 );
function modify_post_table_row( $column_name, $post_id ) {
    $custom_fields = get_post_custom( $post_id );
    switch ($column_name) {
        case 'post_type' :
            $data = get_post_meta( $post_id, 'post_seo_description', true );
            echo '<div id="post_type-'.$post_id.'>'.$data.'</div>';
                    
            //echo '<a style="font-weight:bold;" href="<?php echo admin_url(); ?>post.php?post=<? echo get_the_ID(); ?>&action=edit"><?php echo $data; ?></a><?php
            break;
        default:
    }
}
*/
// Columns
add_filter( 'manage_podcast_posts_columns', 'set_custom_edit_podcast_columns' );
function set_custom_edit_podcast_columns($columns) {
    //unset( $columns['podcast'] );
    $columns['season'] = __( 'Temporada');
    $columns['number'] = __( 'Número');
    return $columns;
}

add_action( 'manage_podcast_posts_custom_column', 'set_custom_edit_podcast_custom_column', 10, 2);
function set_custom_edit_podcast_custom_column( $column, $post_id ) {
    if ((strval($column) == 'number') || ($column == 'season'))
    {
        echo get_post_meta($post_id, strval($column), true);
    }
    else
    {
        echo '1';
    }
}

add_filter( 'manage_evento_posts_columns', 'set_custom_edit_evento_columns' );
function set_custom_edit_evento_columns($columns) {
    //unset( $columns['evento'] );
    $columns['number'] = __( 'Número');
    return $columns;
}

add_action( 'manage_evento_posts_custom_column', 'set_custom_edit_evento_custom_column', 10, 2);
function set_custom_edit_evento_custom_column( $column, $post_id ) {
    if ($column === 'number')
    {
        echo get_post_meta($post_id, $column, true);
    }
}
add_filter( 'manage_edit-podcast_sortable_columns', 'podcast_sortable_columns');
function podcast_sortable_columns( $columns ) {
    $columns['season'] = __( 'Temporada');
    $columns['number'] = __( 'Número');
    return $columns;
}

add_filter( 'manage_edit-evento_sortable_columns', 'evento_sortable_columns');
function evento_sortable_columns( $columns ) {
    $columns['number'] = __( 'Número');
    return $columns;
}

add_action( 'pre_get_posts', 'custom_post_type_custom_orderby' );
function custom_post_type_custom_orderby( $query ) {
    if ( ! is_admin() )
        return;
    $orderby = $query->get( 'orderby');
    if ( $orderby == 'podcast' ) {
        $query->set( 'meta_key', 'number' );
        $query->set( 'orderby', 'meta_value_num' );
    }elseif ( $orderby == 'evento' ) {
        $query->set( 'meta_key', 'number' );
        $query->set( 'orderby', 'meta_value_num' );
    }
}

add_filter( 'pre_get_posts', 'my_get_posts' );
function my_get_posts( $query ) {
    if ( is_home() && $query->is_main_query() )
        $query->set('post_type', array('evento', 'podcast'));

    return $query;
}

/**
 * Unregisters a post type.
 *
 * @param string $post_type Post type to unregister.
 * @return bool|WP_Error True on success, WP_Error if the post type does not exist.
 */
function unregister_post_type_any($post_type){
    global $wp_post_types;

    if (!post_type_exists($post_type))
        return new WP_Error('invalid_post_type', __('Invalid post type.'));

    $post_type_object = get_post_type_object($post_type);

    $post_type_object->remove_supports();
    $post_type_object->remove_rewrite_rules();
    $post_type_object->unregister_meta_boxes();
    $post_type_object->remove_hooks();
    $post_type_object->unregister_taxonomies();

    unset($wp_post_types[$post_type]);

    /**
     * Fires after a post type was unregistered.
     *
     * @param string $post_type Post type identifier.
     */
    do_action('unregistered_post_type', $post_type);

    return true;
}

/*
 * Unregisters the 'Post' built-in post type.
 */
function rc_unregister_post_bpt(){
    unregister_post_type_any('post');
}
add_action('init', 'rc_unregister_post_bpt', 9999, 0);